use std::iter::FromIterator;

pub struct Node<T> {
    data: T,
    next: Option<Box<Node<T>>>,
}

#[derive(Default)]
pub struct SimpleLinkedList<T> {
    head: Option<Box<Node<T>>>,
}

impl<T: Copy> SimpleLinkedList<T> {
    pub fn new() -> Self {
        Self { head: None }
    }

    pub fn len(&self) -> usize {
        let mut length = 0;
        let mut current = &self.head;

        while let Some(node) = current {
            current = &node.next;
            length += 1;
        }

        length
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn push(&mut self, element: T) {
        let new_node = Node {
            data: element,
            next: self.head.take(),
        };
        self.head = Some(Box::new(new_node));
    }

    pub fn pop(&mut self) -> Option<T> {
        self.head.take().map(|mut x| {
            self.head = x.next.take();
            x.data
        })
    }

    pub fn peek(&self) -> Option<&T> {
        self.head.as_ref().map(|x| &x.data)
    }

    pub fn rev(self) -> SimpleLinkedList<T> {
        let mut list = SimpleLinkedList::new();
        let mut current = &self.head;

        while let Some(node) = current {
            list.push(node.data);
            current = &node.next;
        }

        list
    }
}

impl<T: Copy> FromIterator<T> for SimpleLinkedList<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut list = SimpleLinkedList::new();
        for item in iter {
            list.push(item);
        }
        list
    }
}

impl<T: Copy> From<SimpleLinkedList<T>> for Vec<T> {
    fn from(list: SimpleLinkedList<T>) -> Self {
        let mut vec = vec![];
        let mut current = &list.head;

        while let Some(node) = current {
            vec.push(node.data);
            current = &node.next;
        }

        vec.reverse();
        vec
    }
}
