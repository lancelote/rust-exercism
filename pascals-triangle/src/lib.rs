pub struct PascalsTriangle {
    row_count: usize,
}

impl PascalsTriangle {
    pub fn new(row_count: u32) -> Self {
        PascalsTriangle {
            row_count: row_count as usize,
        }
    }

    pub fn rows(&self) -> Vec<Vec<u32>> {
        let mut rows = vec![];

        for i in 0..self.row_count {
            if i == 0 {
                rows.push(vec![1]);
            } else {
                let mut new_row = vec![1];
                let last_row = rows.last().unwrap();

                for j in 0..i {
                    let item = last_row.get(j).unwrap() + last_row.get(j + 1).unwrap_or(&0);
                    new_row.push(item);
                }

                rows.push(new_row);
            }
        }

        rows
    }
}
