#[derive(Debug, PartialEq)]
pub enum Error {
    NotEnoughPinsLeft,
    GameComplete,
}

enum Frame {
    Strike,
    Spare(u16, u16),
    Open(u16, u16),
    Incomplete(u16),
    Extra(u16),
}

#[derive(Default)]
pub struct BowlingGame {
    frames: Vec<Frame>,
    extra: bool,
}

impl BowlingGame {
    pub fn new() -> Self {
        BowlingGame::default()
    }

    pub fn roll(&mut self, pins: u16) -> Result<(), Error> {
        match &self.frames.last() {
            _ if pins > 10 => return Err(Error::NotEnoughPinsLeft),
            Some(Frame::Incomplete(last)) if last + pins > 10 => {
                return Err(Error::NotEnoughPinsLeft)
            }
            Some(Frame::Extra(_)) if !self.extra => return Err(Error::GameComplete),
            Some(Frame::Extra(last)) if *last != 10 && last + pins > 10 => {
                return Err(Error::NotEnoughPinsLeft)
            }
            _ => (),
        }

        match (&self.frames.len(), &self.frames.last()) {
            (10, Some(Frame::Open(_, _))) => return Err(Error::GameComplete),
            (10, Some(Frame::Spare(_, _))) => (),
            (10, Some(Frame::Strike)) => (),
            (11, Some(Frame::Extra(_))) if self.extra => (),
            (11, Some(Frame::Extra(_))) => return Err(Error::GameComplete),
            (12, Some(Frame::Extra(_))) => return Err(Error::GameComplete),
            _ => (),
        }

        match &self.frames.last() {
            Some(&Frame::Strike) => match (&self.frames.len(), pins) {
                (10, _) => {
                    self.frames.push(Frame::Extra(pins));
                    self.extra = true;
                }
                (_, 10) => self.frames.push(Frame::Strike),
                (_, _) => self.frames.push(Frame::Incomplete(pins)),
            },
            Some(&Frame::Spare(_, _)) => match (&self.frames.len(), pins) {
                (10, _) => self.frames.push(Frame::Extra(pins)),
                (_, 10) => self.frames.push(Frame::Strike),
                (_, _) => self.frames.push(Frame::Incomplete(pins)),
            },
            Some(&Frame::Open(_, _)) | None => match pins {
                10 => self.frames.push(Frame::Strike),
                _ => self.frames.push(Frame::Incomplete(pins)),
            },
            Some(&Frame::Incomplete(last)) => {
                self.frames.pop(); // Remove incomplete frame
                match last + pins {
                    10 => self.frames.push(Frame::Spare(last, pins)),
                    _ => self.frames.push(Frame::Open(last, pins)),
                }
            }
            Some(&Frame::Extra(_)) => self.frames.push(Frame::Extra(pins)),
        };

        Ok(())
    }

    fn one_after(&self, i: usize) -> u16 {
        match self.frames[i + 1] {
            Frame::Open(first, _) => first,
            Frame::Spare(first, _) => first,
            Frame::Incomplete(first) => first,
            Frame::Strike => 10,
            Frame::Extra(first) => first,
        }
    }

    fn two_after(&self, i: usize) -> u16 {
        match self.frames[i + 1] {
            Frame::Open(first, second) => first + second,
            Frame::Spare(first, second) => first + second,
            Frame::Incomplete(_) => unreachable!(),
            Frame::Strike => 10 + self.one_after(i + 1),
            Frame::Extra(first) => first + self.one_after(i + 1),
        }
    }

    pub fn score(&self) -> Option<u16> {
        match (self.frames.len(), &self.frames.last()) {
            (frames_number, _) if frames_number < 10 => None,
            (frames_number, _) if frames_number > 12 => None,
            (10, Some(Frame::Strike)) => None,
            (11, Some(Frame::Extra(_))) if self.extra => None,
            (10, Some(Frame::Spare(_, _))) => None,
            _ => {
                let mut result = 0;
                for (i, frame) in self.frames.iter().enumerate() {
                    match frame {
                        Frame::Open(first, second) => result += first + second,
                        Frame::Spare(first, second) => result += first + second + self.one_after(i),
                        Frame::Incomplete(_) => (),
                        Frame::Strike => result += 10 + self.two_after(i),
                        Frame::Extra(_) => (),
                    }
                }
                Some(result)
            }
        }
    }
}
