use std::collections::HashSet;

/// Determine whether a sentence is a pangram.
pub fn is_pangram(sentence: &str) -> bool {
    sentence
        .to_lowercase()
        .chars()
        .filter(|x| x.is_alphabetic() && x.is_ascii())
        .collect::<HashSet<char>>()
        .len()
        == 26
}
