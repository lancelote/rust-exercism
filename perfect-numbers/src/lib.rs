#[derive(Debug, PartialEq, Eq)]
pub enum Classification {
    Abundant,
    Perfect,
    Deficient,
}

pub fn classify(num: u64) -> Option<Classification> {
    let aliquot_sum: u64 = (1..=num / 2).filter(|x| num % x == 0).sum();

    match num {
        0 => None,
        _ if aliquot_sum == num => Some(Classification::Perfect),
        _ if aliquot_sum > num => Some(Classification::Abundant),
        _ if aliquot_sum < num => Some(Classification::Deficient),
        _ => unreachable!(),
    }
}
