use itertools::Itertools;
use std::collections::HashSet;
use unicode_segmentation::UnicodeSegmentation;

fn sorted_chars(word: String) -> String {
    word.graphemes(true).sorted().collect()
}

fn normalize(word: &str) -> String {
    word.to_uppercase()
}

fn is_anagram(word1: &str, word2: &str) -> bool {
    let norm_word1 = normalize(word1);
    let norm_word2 = normalize(word2);
    norm_word1 != norm_word2 && sorted_chars(norm_word1) == sorted_chars(norm_word2)
}

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    possible_anagrams
        .iter()
        .cloned()
        .filter(|&x| is_anagram(word, x))
        .collect::<HashSet<&str>>()
}
