#[derive(Debug)]
pub struct ChessPosition {
    x: i32,
    y: i32,
}

#[derive(Debug)]
pub struct Queen {
    position: ChessPosition,
}

impl ChessPosition {
    pub fn new(x: i32, y: i32) -> Option<Self> {
        if (0..8).contains(&x) && y >= 0 && y < 8 {
            Some(ChessPosition { x, y })
        } else {
            None
        }
    }
}

impl Queen {
    pub fn new(position: ChessPosition) -> Self {
        Self { position }
    }

    pub fn can_attack(&self, other: &Queen) -> bool {
        let dx = self.position.x - other.position.x;
        let dy = self.position.y - other.position.y;
        dx == 0 || dy == 0 || i32::abs(dx) == i32::abs(dy)
    }
}
