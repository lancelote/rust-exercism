use std::collections::HashMap;

#[derive(Default)]
pub struct School {
    data: HashMap<u32, Vec<String>>,
}

impl School {
    pub fn new() -> School {
        School {
            data: HashMap::new(),
        }
    }

    pub fn add(&mut self, grade: u32, student: &str) {
        self.data
            .entry(grade)
            .or_insert_with(Vec::new)
            .push(student.to_string());
    }

    pub fn grades(&self) -> Vec<u32> {
        let mut result: Vec<u32> = self.data.keys().copied().collect();
        result.sort_unstable();
        result
    }

    pub fn grade(&self, grade: u32) -> Option<Vec<String>> {
        if let Some(students) = self.data.get(&grade).cloned().as_mut() {
            students.sort();
            Some(students.clone())
        } else {
            None
        }
    }
}
