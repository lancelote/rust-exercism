use std::collections::HashMap;

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
    match nucleotide {
        'A' | 'C' | 'G' | 'T' => (),
        _ => return Err(nucleotide),
    }

    let mut count: usize = 0;

    for chr in dna.chars() {
        match chr {
            'A' | 'C' | 'G' | 'T' if chr == nucleotide => count += 1,
            'A' | 'C' | 'G' | 'T' => (),
            _ => return Err(chr),
        }
    }

    Ok(count)
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    let count_a = count('A', dna);
    let count_c = count('C', dna);
    let count_g = count('G', dna);
    let count_t = count('T', dna);

    if let Err(e) = count_a {
        return Err(e);
    } else if let Err(e) = count_c {
        return Err(e);
    } else if let Err(e) = count_g {
        return Err(e);
    } else if let Err(e) = count_t {
        return Err(e);
    }

    let mut counts = HashMap::with_capacity(4);
    counts.insert('A', count_a.unwrap());
    counts.insert('C', count_c.unwrap());
    counts.insert('G', count_g.unwrap());
    counts.insert('T', count_t.unwrap());

    Ok(counts)
}
