fn any_alphabetic(message: &str) -> bool {
    message.chars().any(|x| x.is_alphabetic())
}

fn all_upper(message: &str) -> bool {
    any_alphabetic(message) && message.to_uppercase() == message
}

pub fn reply(message: &str) -> &str {
    let message = message.trim();
    match message {
        _ if message.is_empty() => "Fine. Be that way!",
        _ if all_upper(message) && message.ends_with('?') => "Calm down, I know what I'm doing!",
        _ if all_upper(message) => "Whoa, chill out!",
        _ if message.ends_with('?') => "Sure.",
        _ => "Whatever.",
    }
}
