fn wrong_length(isbn: &str) -> bool {
    isbn.chars().filter(|&x| x.is_numeric() || x == 'X').count() != 10
}

fn wrong_x_usage(isbn: &str) -> bool {
    isbn.matches('X').count() > 1 || (isbn.contains('X') && !isbn.ends_with('X'))
}

/// Determines whether the supplied string is a valid ISBN number
pub fn is_valid_isbn(isbn: &str) -> bool {
    if wrong_length(isbn) || wrong_x_usage(isbn) {
        return false;
    }

    let hash = isbn
        .chars()
        .filter(|&x| x.is_numeric() || x == 'X')
        .map(|x| match x {
            'X' => 10,
            _ => x.to_digit(10).unwrap(),
        })
        .rev()
        .enumerate()
        .fold(0, |acc, (i, x)| acc + x * (i as u32 + 1));

    hash % 11 == 0
}
