fn get_one_name(n: u64) -> String {
    String::from(match n {
        0 => "zero",
        1 => "one",
        2 => "two",
        3 => "three",
        4 => "four",
        5 => "five",
        6 => "six",
        7 => "seven",
        8 => "eight",
        9 => "nine",
        _ => unreachable!(),
    })
}

fn get_teen_name(n: u64) -> String {
    String::from(match n {
        10 => "ten",
        11 => "eleven",
        12 => "twelve",
        13 => "thirteen",
        14 => "fourteen",
        15 => "fifteen",
        16 => "sixteen",
        17 => "seventeen",
        18 => "eighteen",
        19 => "nineteen",
        _ => unreachable!(),
    })
}

fn get_round_ten_name(n: u64) -> String {
    String::from(match n / 10 {
        2 => "twenty",
        3 => "thirty",
        4 => "forty",
        5 => "fifty",
        6 => "sixty",
        7 => "seventy",
        8 => "eighty",
        9 => "ninety",
        _ => unreachable!(),
    })
}

fn get_ten_name(n: u64) -> String {
    if n < 10 {
        get_one_name(n % 10)
    } else if (10..20).contains(&n) {
        get_teen_name(n)
    } else if n % 10 == 0 {
        get_round_ten_name(n)
    } else {
        format!("{}-{}", get_round_ten_name(n), get_one_name(n % 10))
    }
}

pub fn get_hundred_name(n: u64) -> String {
    let hundreds = n / 100;
    let tens = n % 100;

    match hundreds {
        0 => get_ten_name(tens),
        _ => match tens {
            0 => format!("{} hundred", get_ten_name(hundreds)),
            _ => format!("{} hundred {}", get_ten_name(hundreds), get_ten_name(tens)),
        },
    }
}

pub fn split_number(n: u64) -> Vec<u64> {
    if n == 0 {
        return vec![0];
    }

    let mut remaining: u64 = n;
    let mut result: Vec<u64> = vec![];

    while remaining != 0 {
        let chunk: u64 = remaining % 1000;
        result.push(chunk);
        remaining /= 1000;
    }

    result
}

pub fn encode(n: u64) -> String {
    let scales = vec![
        "",
        "thousand",
        "million",
        "billion",
        "trillion",
        "quadrillion",
        "quintillion",
    ];
    let chunks = split_number(n);

    let mut result = vec![];
    if !(chunks.len() > 1 && chunks[0] == 0) {
        result.push(get_hundred_name(chunks[0]))
    }

    for i in 1..chunks.len() {
        if chunks[i] != 0 {
            result.push(String::from(scales[i]));
            result.push(get_hundred_name(chunks[i]));
        }
    }

    result.reverse();
    result.join(" ")
}
