use std::collections::HashSet;

pub struct Allergies {
    allergens: HashSet<Allergen>,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Allergen {
    Eggs,
    Peanuts,
    Shellfish,
    Strawberries,
    Tomatoes,
    Chocolate,
    Pollen,
    Cats,
}

impl Allergies {
    pub fn new(score: u32) -> Self {
        let mut normalize_score = score % 256;
        let mut allergens: HashSet<Allergen> = HashSet::new();

        while normalize_score != 0 {
            if normalize_score >= 128 {
                allergens.insert(Allergen::Cats);
                normalize_score -= 128;
            } else if normalize_score >= 64 {
                allergens.insert(Allergen::Pollen);
                normalize_score -= 64;
            } else if normalize_score >= 32 {
                allergens.insert(Allergen::Chocolate);
                normalize_score -= 32;
            } else if normalize_score >= 16 {
                allergens.insert(Allergen::Tomatoes);
                normalize_score -= 16;
            } else if normalize_score >= 8 {
                allergens.insert(Allergen::Strawberries);
                normalize_score -= 8;
            } else if normalize_score >= 4 {
                allergens.insert(Allergen::Shellfish);
                normalize_score -= 4;
            } else if normalize_score >= 2 {
                allergens.insert(Allergen::Peanuts);
                normalize_score -= 2;
            } else {
                allergens.insert(Allergen::Eggs);
                normalize_score -= 1;
            }
        }
        Allergies { allergens }
    }

    pub fn is_allergic_to(&self, allergen: &Allergen) -> bool {
        self.allergens.contains(allergen)
    }

    pub fn allergies(&self) -> Vec<Allergen> {
        let mut result = vec![];
        for allergen in self.allergens.iter() {
            result.push(allergen.clone());
        }
        result
    }
}
