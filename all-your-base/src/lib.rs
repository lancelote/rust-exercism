#[derive(Debug, PartialEq)]
pub enum Error {
    InvalidInputBase,
    InvalidOutputBase,
    InvalidDigit(u32),
}

pub fn convert(number: &[u32], from_base: u32, to_base: u32) -> Result<Vec<u32>, Error> {
    if from_base < 2 {
        return Err(Error::InvalidInputBase);
    }
    if to_base < 2 {
        return Err(Error::InvalidOutputBase);
    }

    let mut decimal = 0;
    for digit in number {
        if digit >= &from_base {
            return Err(Error::InvalidDigit(*digit));
        }
        decimal = from_base * decimal + digit
    }

    let mut digits = vec![];
    while decimal > 0 {
        digits.push(decimal % to_base);
        decimal /= to_base
    }
    digits.reverse();

    Ok(digits)
}
