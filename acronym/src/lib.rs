pub fn abbreviate(phrase: &str) -> String {
    let mut result = String::new();
    let mut last = ' ';

    for current in phrase.replace("'", "").chars() {
        #[allow(clippy::suspicious_operation_groupings)]
        if (!last.is_alphabetic() && current.is_alphabetic())
            || (last.is_lowercase() && current.is_uppercase())
        {
            result.push(current.to_ascii_uppercase());
        }
        last = current;
    }

    result
}
