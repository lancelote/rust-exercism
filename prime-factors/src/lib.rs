pub fn factors(n: u64) -> Vec<u64> {
    let mut remaining: u64 = n;
    let mut result: Vec<u64> = vec![];
    let mut factor: u64 = 2;

    while remaining != 1 {
        if remaining % factor == 0 {
            remaining /= factor;
            result.push(factor);
        } else {
            factor += 1;
        }
    }

    result
}
