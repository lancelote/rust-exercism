pub fn build_proverb(list: &[&str]) -> String {
    list.iter()
        .enumerate()
        .map(|(i, v)| match i {
            _ if i == list.len() - 1 => format!("And all for the want of a {}.", list[0]),
            _ => format!("For want of a {} the {} was lost.", v, list[i + 1]),
        })
        .collect::<Vec<String>>()
        .join("\n")
}
