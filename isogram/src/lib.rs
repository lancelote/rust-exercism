use std::collections::HashSet;

pub fn check(candidate: &str) -> bool {
    let alphabetic_char_set: HashSet<char> = candidate
        .to_lowercase()
        .chars()
        .filter(|x| x.is_alphabetic())
        .collect();
    let alphabetic_char_count = candidate.chars().filter(|x| x.is_alphabetic()).count();
    alphabetic_char_count == alphabetic_char_set.len()
}
