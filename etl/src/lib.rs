use std::collections::BTreeMap;

pub fn transform(h: &BTreeMap<i32, Vec<char>>) -> BTreeMap<char, i32> {
    let mut new_format = BTreeMap::new();

    for (key, values) in h.iter() {
        for value in values {
            new_format.insert(value.to_ascii_lowercase(), *key);
        }
    }

    new_format
}
