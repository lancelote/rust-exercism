pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let mut saddle_points = vec![];

    for (row_id, row) in input.iter().enumerate() {
        for (col_id, item) in row.iter().enumerate() {
            let check_row = row.iter().all(|x| item >= x);
            let check_col = input.iter().map(|x| &x[col_id]).all(|x| item <= x);

            if check_row && check_col {
                saddle_points.push((row_id, col_id));
            }
        }
    }

    saddle_points
}
