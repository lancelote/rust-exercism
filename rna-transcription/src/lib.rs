#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, PartialEq)]
pub struct DNA {
    dna: String,
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, PartialEq)]
pub struct RNA {
    rna: String,
}

impl DNA {
    pub fn new(dna: &str) -> Result<DNA, usize> {
        for (i, chr) in dna.chars().enumerate() {
            match chr {
                'G' | 'C' | 'T' | 'A' => (),
                _ => return Err(i),
            }
        }

        Ok(DNA {
            dna: String::from(dna),
        })
    }

    pub fn into_rna(self) -> RNA {
        RNA {
            rna: self
                .dna
                .chars()
                .map(|x| match x {
                    'G' => 'C',
                    'C' => 'G',
                    'T' => 'A',
                    'A' => 'U',
                    _ => x,
                })
                .collect(),
        }
    }
}

impl RNA {
    pub fn new(rna: &str) -> Result<RNA, usize> {
        for (i, chr) in rna.chars().enumerate() {
            match chr {
                'C' | 'G' | 'A' | 'U' => (),
                _ => return Err(i),
            }
        }

        Ok(RNA {
            rna: String::from(rna),
        })
    }
}
