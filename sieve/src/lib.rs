use std::collections::HashSet;

pub fn primes_up_to(upper_bound: u64) -> Vec<u64> {
    let mut primes = vec![];
    let mut not_primes: HashSet<u64> = HashSet::new();

    for number in 2..=upper_bound {
        if !not_primes.contains(&number) {
            primes.push(number);
            for i in 2..=upper_bound / number {
                not_primes.insert(number * i);
            }
        }
    }

    primes
}
