fn plural(n: u32) -> String {
    if n == 1 {
        String::from("")
    } else {
        String::from("s")
    }
}

pub fn first_line(n: u32) -> String {
    if n == 0 {
        String::from("No more bottles of beer on the wall, no more bottles of beer.")
    } else {
        format!(
            "{n} bottle{s} of beer on the wall, {n} bottle{s} of beer.",
            n = n,
            s = plural(n)
        )
    }
}

fn second_line(n: u32) -> String {
    if n == 0 {
        String::from("Go to the store and buy some more, 99 bottles of beer on the wall.")
    } else if n == 1 {
        String::from("Take it down and pass it around, no more bottles of beer on the wall.")
    } else {
        format!(
            "Take one down and pass it around, {n} bottle{s} of beer on the wall.",
            n = n - 1,
            s = plural(n - 1)
        )
    }
}

pub fn verse(n: u32) -> String {
    format!("{}\n{}\n", first_line(n), second_line(n))
}

pub fn sing(start: u32, end: u32) -> String {
    (end..=start)
        .rev()
        .map(verse)
        .collect::<Vec<String>>()
        .join("\n")
}
