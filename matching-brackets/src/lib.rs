pub fn brackets_are_balanced(string: &str) -> bool {
    let mut stack = vec![];

    for current in string.chars() {
        match current {
            '[' | '{' | '(' => stack.push(current),
            ']' | '}' | ')' => {
                let expected = match current {
                    ']' => '[',
                    '}' => '{',
                    ')' => '(',
                    _ => unreachable!(),
                };
                if stack.pop() != Some(expected) {
                    return false;
                }
            }
            _ => (),
        }
    }
    stack.is_empty()
}
